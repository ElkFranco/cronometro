from Hora import *
from Minuto import *
from Segundo import *
from Decima import *

"""
s = Segundo()

for i in range(120):
    s.avanzar()
    print s.getValor()
"""

class Cronometro():
    def __init__(self):
        self.h = Hora()
        self.m = Minuto()
        self.s = Segundo()
        self.d = Decima()
                        
    def avanzar(self):
        self.d.avanzar()
        if self.d.getValor() == 0:
            self.s.avanzar()
            if self.s.getValor() == 0:
                self.m.avanzar()
                if self.m.getValor() == 0:
                    self.h.avanzar()

    def getTiempo(self):
        #return str(self.h.getValor())+":"+str(self.m.getValor())+":"+str(self.s.getValor())+":"+str(self.d.getValor())
        self.valoraux = ""
        if self.h.getValor() < 10:
            self.valoraux = str(self.valoraux) + "0" + str(self.h.getValor()) + ":"                
        else:
            self.valoraux = str(self.valoraux) + str(self.h.getValor()) + ":"
        if self.m.getValor() < 10:
            self.valoraux = str(self.valoraux) + "0" + str(self.m.getValor()) + ":"                
        else:
            self.valoraux = str(self.valoraux) + str(self.m.getValor()) + ":"
        if self.s.getValor() < 10:
            self.valoraux = str(self.valoraux) + "0" + str(self.s.getValor()) + ":"                
        else:
            self.valoraux = str(self.valoraux) + str(self.s.getValor()) + ":"
        if self.d.getValor() < 10:
            self.valoraux = str(self.valoraux) + "0" + str(self.d.getValor()) + ":"                
        else:
            self.valoraux = str(self.valoraux) + str(self.d.getValor()) + ":"           
        
        #self.valoraux = str(self.h.getValor())+":"+str(self.m.getValor())+":"+str(self.s.getValor())+":"+str(self.d.getValor())
        return self.valoraux
        #return 0
    
    def retroceder(self):
        self.d.retroceder()
        if self.d.getValor() == self.d.getTope():
            self.s.retroceder()
            if self.s.getValor() == self.s.getTope():
                self.m.retroceder()
                if self.m.getValor() == self.m.getTope():
                    self.h.retroceder()


    def setTiempo(self):
        #self.valoraux = input ("Digite H ")
        #self.h.setValor(self.valoraux)
        self.h.setValor(input ("Digite H (0 - 23) : "))
        self.m.setValor(input ("Digite M (0 - 59) : "))
        self.s.setValor(input ("Digite S (0 - 59) : "))
        self.d.setValor(input ("Digite D (0 - 9) : "))
        
