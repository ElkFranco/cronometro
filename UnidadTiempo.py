class UnidadTiempo():
    def _init__(self):
        self.valor = 0
        self.tope = 0

    def avanzar(self):
        if self.valor < self.tope:
            self.valor += 1
        else:
            self.valor = 0

    def retroceder(self):
        if self.valor > 0:
            self.valor -= 1
        else:
            self.valor = self.tope

    def getTope(self):
        return self.tope
    
    def getValor(self):
        #if self.valor < 10:
            #return "0" + int(self.valor)
        #else:
            return self.valor

    def setValor(self,a):
        self.valor = a
        return self.valor    
    
# Por que (self,...,xxx)
